package com.innominds.ds;

public class Q3Employee {

	private int id;
	private String name;
	private String email;
	private char gender;
	private float salary;
	public Q3Employee(int id, String name, String email, char gender, float salary) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.gender = gender;
		this.salary = salary;
	}
	public Q3Employee() {
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public char getGender() {
		return gender;
	}
	public void setGender(char gender) {
		this.gender = gender;
	}
	public float getSalary() {
		return salary;
	}
	public void setSalary(float salary) {
		this.salary = salary;
	}
	
	public void getQ3EmployeeDetails() {
		System.out.println(
			"*****Q3Employee Details*****\n"
			+"Id :"+id
			+"\nName   :"+name
			+"\nEmail  :"+email
			+"\nGender :"+gender
			+"\nSalary :"+salary
		);
		
	}
	@Override
	public String toString() {
		return "Q3Employee [id=" + id + ", name=" + name + ", email=" + email + ", gender=" + gender + ", salary="
				+ salary + "]";
	}
	
}
