
package com.innominds.ds;
/*
 * 1.a
 * Write a JAVA program that creates threads by extending Thread class .First thread 
 * display “Good Morning “every 1 sec, the second thread displays “Hello “every 2 
 * seconds and the third display “Welcome” every 3 seconds ,(Repeat the same by 
 * implementing Runnable)
 */
class ResourceLock {
	public volatile int flag = 1;
}

class ThreadA extends Thread {
	ResourceLock lock;
	 
	 ThreadA(ResourceLock lock){
	  this.lock = lock;
	 }
	 
	 @Override
	 public void run() {
	 
	  try{
	   synchronized (lock) {
	 
	    for (int i = 0; i < 100; i++) {
	 
	     while(lock.flag!=1){
	      lock.wait();
	     }
	     Thread.sleep(1000);
	     System.out.println("Good Morning");
	     lock.flag = 2;
	     lock.notifyAll();
	    }
	 
	   }
	  }catch (Exception e) {
	   System.out.println("Exception 1 :"+e.getMessage());
	  }
	 
	 }
}

class ThreadB extends Thread {

	 ResourceLock lock;
	 
	 ThreadB(ResourceLock lock){
	  this.lock = lock;
	 }
	 
	 @Override
	 public void run() {
	 
	  try{
	   synchronized (lock) {
	 
	    for (int i = 0; i < 100; i++) {
	 
	     while(lock.flag!=2){
	      lock.wait();
	     }
	     Thread.sleep(2000);
	     System.out.println("Hello");
	     lock.flag = 3;
	     lock.notifyAll();
	    }
	 
	   }
	  }catch (Exception e) {
	   System.out.println("Exception 2 :"+e.getMessage());
	  }
	 
	 }
}

class ThreadC extends Thread {
	ResourceLock lock;
	 
	 ThreadC(ResourceLock lock){
	  this.lock = lock;
	 }
	 
	 @Override
	 public void run() {
	 
	  try{
	   synchronized (lock) {
	 
	    for (int i = 0; i < 100; i++) {
	 
	     while(lock.flag!=3){
	      lock.wait();
	     }
	     Thread.sleep(3000);
	     System.out.println("Welcome");
	     lock.flag = 1;
	     lock.notifyAll();
	    }
	 
	   }
	  }catch (Exception e) {
	   System.out.println("Exception 3 :"+e.getMessage());
	  }
	 
	 }
}

public class Q1TheadDemo {

	public static void main(String[] args) throws InterruptedException {
		 ResourceLock lock = new ResourceLock();
		 
	        ThreadA a=new ThreadA(lock);
	        ThreadB b=new ThreadB(lock);
	        ThreadC c=new ThreadC(lock);
	 
	        a.start();
	        b.start();
	        c.start();
	}
}
