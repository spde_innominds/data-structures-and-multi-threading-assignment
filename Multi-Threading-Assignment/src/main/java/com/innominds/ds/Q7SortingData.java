package com.innominds.ds;

/*
 * 7. Write a program to sort the any type of data by using inner class without using 
 * predefined? 
 * Ex : class Sorting {
 * 			class DataSorting(){
				// write logic here
		  	}
		}
 */
import java.util.Arrays;
import java.util.Scanner;

public class Q7SortingData {

	class Sorting {
		public String[] sortig_data(String[] data) {
			String temp;
			for (int i = 0; i < data.length; i++) {
				for (int j = i + 1; j < data.length; j++) {
					if (data[i].compareTo(data[j]) > 0) {
						temp = data[i];
						data[i] = data[j];
						data[j] = temp;
					}
				}
			}
			return data;
		}
	}

	public static void main(String[] args) {
		System.out.println("Enter no of elements ");
		Scanner s = new Scanner(System.in);
		int n = s.nextInt();
		String data[] = new String[n];
		for (int i = 0; i < n; i++) {
			data[i] = s.next();
		}
		Sorting obj = new Q7SortingData().new Sorting();
		System.out.println("Elements data after sorting");
		System.out.println(Arrays.toString(obj.sortig_data(data)));
	}
}
