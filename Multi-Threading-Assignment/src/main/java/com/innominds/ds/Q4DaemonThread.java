
package com.innominds.ds;
/*
 * 4. Write a Program illustrating Daemon Threads
 */
class DaemonThread extends Thread{
	@Override
	public void run() {
		if(Thread.currentThread().isDaemon()) {
			System.out.println("Current "+Thread.currentThread().getName()+" is Daemon Thread");
		}
		else {
			System.out.println("Current "+Thread.currentThread().getName()+" is User Thread");
		}
	}
}
public class Q4DaemonThread{
	public static void main(String args[]) {
		DaemonThread thread = new DaemonThread();
		DaemonThread thread1 = new DaemonThread();
		DaemonThread thread2 = new DaemonThread();
		thread1.start();
		thread.start();
		thread2.setDaemon(true);
		thread2.start();
	}
	
}