
package com.innominds.ds;
/*
 * 2. Write down a java program to print even and odd numbers series respectively from 
 * two threads: 
 * t1 and t2 synchronizing on a shared object
 * Let t1 print message “ping — >” and t2 print message “,—-pong”.
 * Take as command line arguments, the following inputs to the program:
 * Sleep Interval for thread t1
 * Sleep Interval for thread t2
 * Message per cycle 
 * No of cycles
 */
public class Q2EvenOdd{
	static int current=0;
	public static void main(String args[]) {
		//int current =0;
		int time1 = Integer.parseInt(args[0]);
		int time2 = Integer.parseInt(args[1]);
		
		String even = args[2];
		String odd = args[3];
		
		int N = Integer.parseInt(args[4]);
		
		Thread thread1 = new Thread(new Runnable() {

			@Override
			public void run() {
				while(current<=N) {
					if(current%2==0) {
						System.out.println(Thread.currentThread().getName()+" even -"+current);
						current++;
					}
					try {
						Thread.sleep(time1);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				
			}
			
		},even);
		
		Thread thread2 = new Thread(new Runnable() {

			@Override
			public void run() {
				while(current<=N) {
					if(current%2==1) {
						System.out.println(Thread.currentThread().getName()+" odd -"+current);
						current++;
					}
					try {
						Thread.sleep(time2);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			
		},odd);
		
		//thread1.setName(even);
		thread1.start();
		//thread2.setName(odd);
		thread2.start();
	}
}
