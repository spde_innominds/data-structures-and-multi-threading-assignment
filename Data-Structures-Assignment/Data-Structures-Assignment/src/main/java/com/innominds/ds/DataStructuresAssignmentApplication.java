package com.innominds.ds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataStructuresAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(DataStructuresAssignmentApplication.class, args);
	}

}
