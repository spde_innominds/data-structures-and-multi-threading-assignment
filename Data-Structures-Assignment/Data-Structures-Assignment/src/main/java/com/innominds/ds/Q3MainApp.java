/* Create an application for Q3Employee management having following classes:
 * a. Create an Q3Employee class with following attributes and behaviours : 
 * i. EmpId Int
 * ii. EmpName String
 * iii. EmpEmail String
 * iv. EmpGender char
 * v. EmpSalary float
 * vi. GetQ3EmployeeDetails() -> prints Q3Employee details
 * b. Create one more class Q3EmployeeDB which has the following methods.
 * i. boolean addQ3Employee(Q3Employee e)
 * ii. boolean deleteQ3Employee(int empId)
 * iii. String showPaySlip(int empId)
 * iv. Q3Employee[] listAll()
 * Do implementation of the above application with below two methods :
 * 1. Use an ArrayList which will be used to store the Q3Employees and use enumeration/iterator to 
 * process the Q3Employees.
 * 2. Use a TreeSet Object to store Q3Employees on the basis of their EmpId and use 
 * enumeration/iterator to process Q3Employees.
 */
 
package com.innominds.ds;

import java.util.List;

public class Q3MainApp {

	public static void main(String[] args) {
		
		Q3EmployeeDB empdb = new Q3EmployeeDB();
	
		Q3Employee emp1 = new Q3Employee(100,"John","john@innominds.com",'M',20_24_250);
		Q3Employee emp2 = new Q3Employee(102,"Rosy","rosy@innominds.com",'F',25_12_125);
		Q3Employee emp3 = new Q3Employee(101,"Justin","justin@innominds.com",'M',24_23_500);
		
		//printing all Q3Employee details
		emp1.getQ3EmployeeDetails();
		//adding Q3Employee to list
		System.out.print("Added new Q3Employeee to list :");
		System.out.println(empdb.addQ3Employee(emp1));
		System.out.print("Added new Q3Employeee to list :");
		System.out.println(empdb.addQ3Employee(emp2));
		System.out.print("Added new Q3Employeee to list :");
		System.out.println(empdb.addQ3Employee(emp3));
		List<Q3Employee> Q3Employees = empdb.listAll();
		System.out.println("\n***All Q3Employees***\n");
		Q3Employees.stream().forEach(System.out::println);
		//showing Q3Employee payslip
		System.out.println(empdb.showPaySlip(100));
		//deleting Q3Employee from list
		int empId=100;
		System.out.print("Deleted existing Q3Employee with id "+empId+" from list :");
		System.out.println(empdb.deleteEmp(empId));
		//returing all the Q3Employees into list
		System.out.println("\n***All Q3Employees***\n");
		Q3Employees.stream().forEach(System.out::println);
	}

}
