package com.innominds.ds;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Q3EmployeeDB {

	ArrayList<Q3Employee> Q3EmployeeList = new ArrayList<>();
	
	boolean addQ3Employee(Q3Employee Q3Employee) {

		Q3EmployeeList.add(Q3Employee);
		if (!Q3EmployeeList.contains(Q3Employee)) {
			return false;
		}
		return true;
	}

	boolean deleteQ3Employee(int empId) {

		for (Q3Employee emp : Q3EmployeeList) {
			if (emp.getId() == empId) {
				Q3EmployeeList.remove(emp);
				break;
			}
		}
		return !Q3EmployeeList.stream().anyMatch(x->x.getId()==empId);
	}
	boolean deleteEmp(int empId) {

		Iterator iterator = Q3EmployeeList.iterator();
		while(iterator.hasNext()) {
			Q3Employee emp = (Q3Employee)iterator.next();
			if(emp.getId()==empId) {
				iterator.remove();
				break;
			}
		}
		return !Q3EmployeeList.stream().anyMatch(x->x.getId()==empId);
	}
	String showPaySlip(int empId) {
		Q3Employee Q3Employee = new Q3Employee();
		for (Q3Employee emp : Q3EmployeeList) {
			if (emp.getId() == empId) {
				Q3Employee = emp;
				break;
			}
		}
		return "***Q3Employee Pay Slip***\nQ3Employee Salary with empId "+empId+" is Rs."+Q3Employee.getSalary();	
	}
	
	List<Q3Employee> listAll(){
		return Q3EmployeeList;
	}
}
