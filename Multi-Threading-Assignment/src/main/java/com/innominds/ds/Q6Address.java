package com.innominds.ds;

import java.io.Serializable;

public class Q6Address implements Serializable{
	
	private static final long serialVersionUID = 12345L;
	private int dno;
	private String street;
	private String state;
	public Q6Address(int dno, String street, String state) {
		this.dno = dno;
		this.street = street;
		this.state = state;
	}
	@Override
	public String toString() {
		return "Q6Address [dno=" + dno + ", street=" + street + ", state=" + state + "]";
	}
	
}
