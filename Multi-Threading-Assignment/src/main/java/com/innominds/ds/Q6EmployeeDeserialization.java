package com.innominds.ds;

import java.io.BufferedInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class Q6EmployeeDeserialization{

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		File file = new File("subbu.txt");
		BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(file));
		
		ObjectInputStream in =new ObjectInputStream(inputStream);
		//Emp e = (Emp)in.readObject();
		Q6Employee e ; 
		try {
			while((e=(Q6Employee)in.readObject())!=null) {
				System.out.println(e.toString());
			}
		}catch(EOFException ex) {}
		
		System.out.println("Deseralization of object was done!\nWe used transient keyword for salary so, salary won't transfered from serialized object");
	}

}
