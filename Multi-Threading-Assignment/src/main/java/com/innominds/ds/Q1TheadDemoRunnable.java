
package com.innominds.ds;
/*
 * 1.b
 * Write a JAVA program that creates threads by extending Thread class .First thread 
 * display “Good Morning “every 1 sec, the second thread displays “Hello “every 2 
 * seconds and the third display “Welcome” every 3 seconds ,(Repeat the same by 
 * implementing Runnable)
 */
class ResourceLock1 {
	public volatile int flag = 1;
}
class Thread1 implements Runnable {
	ResourceLock1 lock;

	Thread1(ResourceLock1 lock) {
		this.lock = lock;
	}

	@Override
	public void run() {

		try {
			synchronized (lock) {

				for (int i = 0; i < 100; i++) {

					while (lock.flag != 1) {
						lock.wait();
					}
					Thread.sleep(1000);
					System.out.println("Good Morning");
					lock.flag = 2;
					lock.notifyAll();
				}

			}
		} catch (Exception e) {
			System.out.println("Exception 1 :" + e.getMessage());
		}

	}
}

class Thread2 extends Thread {

	ResourceLock1 lock;

	Thread2(ResourceLock1 lock) {
		this.lock = lock;
	}

	@Override
	public void run() {

		try {
			synchronized (lock) {

				for (int i = 0; i < 100; i++) {

					while (lock.flag != 2) {
						lock.wait();
					}
					Thread.sleep(2000);
					System.out.println("Hello");
					lock.flag = 3;
					lock.notifyAll();
				}

			}
		} catch (Exception e) {
			System.out.println("Exception 2 :" + e.getMessage());
		}

	}
}

class Thread3 extends Thread {
	ResourceLock1 lock;

	Thread3(ResourceLock1 lock) {
		this.lock = lock;
	}

	@Override
	public void run() {

		try {
			synchronized (lock) {

				for (int i = 0; i < 100; i++) {

					while (lock.flag != 3) {
						lock.wait();
					}
					Thread.sleep(3000);
					System.out.println("Welcome");
					lock.flag = 1;
					lock.notifyAll();
				}

			}
		} catch (Exception e) {
			System.out.println("Exception 3 :" + e.getMessage());
		}

	}
}

public class Q1TheadDemoRunnable {

	public static void main(String[] args) throws InterruptedException {
		ResourceLock1 lock = new ResourceLock1();
		Thread1 a = new Thread1(lock);
		Thread t1 = new Thread(a);
		Thread2 b = new Thread2(lock);
		Thread t2 = new Thread(b);
		Thread3 c = new Thread3(lock);
		Thread t3 = new Thread(c);

		t1.start();
		t2.start();
		t3.start();
	}
}
