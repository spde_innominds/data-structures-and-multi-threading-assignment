package com.innominds.ds;

import java.util.Scanner;

public class Q5ReservationSystem {
	Scanner sc=new Scanner(System.in);
	boolean[] seats;
	public boolean[] seatCapacity(int n) {
		seats = new boolean[n];
		return seats;
	}

	public void bookSeat() {
		System.out.println("Please select '1' for “Smoking” & select '2' for “Non-Smoking” and '0' to Quit");
		System.out.print("Choose Valid Class : ");
		String passengerClass = sc.next();

		if (passengerClass.equals("1") || passengerClass.equals("2") || passengerClass.equals("0")) {
			if (passengerClass.equals("1")) {
				smokeClassBooking();
			} else if(passengerClass.equals("2")){
				nonSmokeClassBooking();
			}else {
				System.exit(0);
			}
		} else
			System.out.println("Enter valid Number to Select Class");
		System.out.println();
	}
	public void smokeClassBooking() {
		for (int i = 0; i <seats.length/2; i++) {
			if (seats[i] == false) {
				seats[i] = true;
				System.out.println("Seat Booked Succesfully in Smoking Class. Your seat no." + (i + 1));
				System.out.println("\nSeats in Smoking section are");
				for(int k=0;k<seats.length/2;k++) {
					if(!seats[k]) {
						System.out.print((k+1)+",");
					}
				}
				System.out.println();
				break;
			} else if (seats[(seats.length/2)-1] == true) {
				// If both class are booked
				if (seats[seats.length-1] == true) {
					System.out.println();
					System.out.println("All seats were booked.Next flight departs in 3 hours");
					System.exit(0);
					break;
				} else {
					System.out.println(
							"Smoking Class seat was Booked. Would you like to opt for Non Smoking class please select 'y' for Yes and 'n' for No");
					String selection = sc.next();
					if (selection.equals("y") || selection.equals("n")) {
						if (selection.equals("y")) {
							nonSmokeClassBooking();
							start();
						} else {
							System.out.println("Next flight departs in 3 hours");
							System.exit(0);
						}
					} else {
						System.out.println("Choose Valid Option");
					}
				}
			}
		}
		
	}
	public void nonSmokeClassBooking() {
		for (int i =(seats.length/2); i <seats.length; i++) {
			if (seats[i] == false) {
				seats[i] = true;
				System.out.println("Seat Booked Succesfully in NonSmoking Class. Your seat no." + (i + 1));
				System.out.println("\nSeats in Non-Smoking section are");
				for(int k=seats.length/2;k<seats.length;k++) {
					if(!seats[k]) {
						System.out.print((k+1)+",");
					}
				}
				System.out.println();
				break;

			} else if (seats[seats.length-1] == true) {
				if (seats[(seats.length/2)-1] == true) {
					// if both classes are fully booked
					System.out.println();
					System.out.println("All seats are booked.Next flight leaves in 3 hours");
					System.exit(0);
					break;
				} else {
					System.out.println(
							"NonSmoking Class seats are Booked. Would you like to opt for Smoking class please select 'y' for Yes and 'n' for No");
					String selection = sc.next();
					if (selection.equals("y") || selection.equals("n")) {
						if (selection.equals("y")) {
							smokeClassBooking();
							start();
						} else {
							System.out.println("Next flight leaves in 3 hours");
							System.exit(0);
						}
					} else {
						System.out.println("Choose Valid Option");
					}
				}
			}
		}
	}
	public void start() {
		while (true) {
			bookSeat();
		}
	}
}
