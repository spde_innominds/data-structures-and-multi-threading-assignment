package com.innominds.ds;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class Q6EmployeeSerialization {
	public static void main(String[] args) throws IOException {
		File file = new File("subbu.txt");
		
		BufferedOutputStream o =
				new BufferedOutputStream(new FileOutputStream(file));
		ObjectOutputStream os =
				new ObjectOutputStream(o);
		
		Q6Employee e1 = new Q6Employee(101 , "subbu kannepalli" , 10_20_345.50 , 
				new Q6Address(10, "Indira Nagar" , "TS"));
		os.writeObject(e1);
		os.close();
		System.out.println("Serialization done successfully!");

	}
}
