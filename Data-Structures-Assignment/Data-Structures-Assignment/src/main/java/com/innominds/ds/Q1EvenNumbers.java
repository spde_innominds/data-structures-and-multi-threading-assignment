/*
 * Develop a java class with a method saveEvenNumbers(int N) using ArrayList to store even numbers 
 * from 2 to N, where N is a integer which is passed as a parameter to the method saveEvenNumbers(). 
 * The method should return the ArrayList (A1) created. 
 * In the same class create a method printEvenNumbers()which iterates through the arrayList A1 in step 
 * 1, and It should multiply each number with 2 and display it in format 4,8,12….2*N. and add these 
 * numbers in a new ArrayList (A2). The new ArrayList (A2) created needs to be returned. 
 * Create a method printEvenNumber(int N) parameter is a number N. This method should search the 
 * arrayList (A1) for the existence of the number ‘N’ passed. If exists it should return the Number else 
 * return zero.
 * Hint: Use instance variable for storing the ArrayList A1 and A2. 
 * NOTE: You can test the methods using a main method.
 */
package com.innominds.ds;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Q1EvenNumbers {

	ArrayList<Integer> A1 = new ArrayList<>();
	public ArrayList<Integer> saveEvenNumbers(int N){
		A1 = (ArrayList<Integer>) Stream.iterate(1,x->x+1).limit(N).filter(x->x%2==0).sorted().collect(Collectors.toList());
		return A1;
	}
	
	public ArrayList<Integer> printEvenNumbers(){
		ArrayList<Integer> A2= new ArrayList<>();
		A2 = (ArrayList<Integer>) A1.stream().map(x->x*2).sorted().collect(Collectors.toList());
		return A2;
	}
	
	public int printEvenNumber(int N) {
		int evenNumber = 0;
		evenNumber = A1.stream().filter(x->x==N).findAny().orElse(0);
		return evenNumber;
	}
	
	public static void main(String args[]) {
		Scanner s = new Scanner(System.in);
		ArrayList<Integer> result ;
		Q1EvenNumbers obj = new Q1EvenNumbers();
		obj.saveEvenNumbers(10);
		
		result = obj.printEvenNumbers();
		System.out.print("Enter number upto save even numbers:");
		int N=s.nextInt();
		obj.saveEvenNumbers(N);
		System.out.print("Enter a number :");
		int num = s.nextInt();
		int evenNumber = obj.printEvenNumber(num);
		System.out.println("Output :"+evenNumber);
		
	}
}
