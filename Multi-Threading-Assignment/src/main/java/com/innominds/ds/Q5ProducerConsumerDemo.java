
package com.innominds.ds;

/*
 * 5. Write a JAVA program to demonstrate Producer Consumer Problem. 
 * Also demonstrate how synchronization is used to resolve it
 */
import java.util.LinkedList;

class ProducerConsumer {

	LinkedList<Integer> items = new LinkedList<>();
	int capacity=2;
	int val = 1;
	public void produce() throws InterruptedException {
		
		while(true) {
			synchronized(this) {
				while(items.size()==capacity) {
					wait();
				}
				System.out.println("Producer produced item-"+val);
				items.add(val++);
				notify();
				Thread.sleep(1000);
			} 
		}
	}
	public void consume() throws InterruptedException {
		while(true) {
			synchronized(this) {
				while(items.size()==0) {
					wait();
				}
				int val = items.removeFirst();
				System.err.println("Consumer consumed item-"+val);
				notify();
				Thread.sleep(1000);
			}
		}
	}
}
public class Q5ProducerConsumerDemo{
	public static void main(String args[]) throws InterruptedException {
		ProducerConsumer pc = new ProducerConsumer();
		//producer thread
		Thread procuder = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					pc.produce();
				}
				catch(InterruptedException e) {
					e.printStackTrace();
				}
			}
			
		});
		//consumer thread
		Thread consumer = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					pc.consume();
				}
				catch(InterruptedException e) {
					e.printStackTrace();
				}
				
			}
			
		});
		
		//starting the producer and consumer threads
		procuder.start();
		
		consumer.start();
		
		//executing producer thread first and then consumer thread
		procuder.join();
		consumer.join();
		
	}
}
