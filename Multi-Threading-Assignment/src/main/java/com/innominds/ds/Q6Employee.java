package com.innominds.ds;

import java.io.Serializable;

public class Q6Employee implements Serializable{
	
	private static final long serialVersionUID=123456789L;
	private int id;
	private String name;
	private static String company="innominds";
	private transient double salary;
	private Q6Address address;
	public Q6Employee(int id, String name, double salary , Q6Address address) {
		this.id = id;
		this.name = name;
		this.salary = salary;
		this.address = address;
	}
	/*
	@Override
	public String toString() {
		return "Emp [id=" + id + ", name=" + name + ", salary=" + salary + ", company=" + company+ "]";
	}
	
	*/
	@Override
	public String toString() {
		return "Emp [id=" + id + ", name=" + name + ", salary=" + salary + ", address=" + address + "]";
	}
	
	
	
}
