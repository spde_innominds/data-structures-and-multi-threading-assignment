package com.innominds.ds;

class MyThread1 extends Thread{
	Thread thread;
	String name;
	int priority;
	int count = 1;
	MyThread1(String name , int p){
		this.name = name;
		this.priority = p;
		thread = new Thread(this,name);
		thread.setPriority(priority);
		System.out.println(thread.getName()+" started ");
		thread.start();
		
	}
	@Override
	public void run() {
		if(thread.getPriority()>=8) {
			try {
				Thread.sleep(5000);
				System.out.println(thread.getName()+" is alive? "+thread.isAlive());
			}catch (InterruptedException e) {
				e.printStackTrace();
			}
			while(thread.isAlive() && count<=5) {
				System.out.println(Thread.currentThread().getName()+" count to : "+count);
				count++;
			}
		}
		
	}
}
public class Q3ThreadPrioity {
	public static void main(String[] args) {
		MyThread1 thread1 = new MyThread1("Thread-1",1);
		MyThread1 thread2 = new MyThread1("Thread-2",2);
		MyThread1 thread3 = new MyThread1("Thread-3",3);
		MyThread1 thread4 = new MyThread1("Thread-4",8);
		MyThread1 thread5 = new MyThread1("Thread-5",9);
		System.out.println(thread1.getName()+" is alive? "+thread1.isAlive());
		System.out.println(thread2.getName()+" is alive? "+thread2.isAlive());
		System.out.println(thread3.getName()+" is alive? "+thread3.isAlive());
		System.out.println(thread4.getName()+" is alive? "+thread4.isAlive());
		System.out.println(thread5.getName()+" is alive? "+thread5.isAlive());
	}
	
}
